use anyhow::Error;
use rsa::RSAPrivateKey;
use rsa_pem::KeyExt;

#[derive(Clone, Debug, thiserror::Error)]
#[error("Error generating key")]
pub struct MyError;

fn main() -> Result<(), Error> {
    let mut rng = rand::thread_rng();

    let key = RSAPrivateKey::new(&mut rng, 2048).map_err(|_| MyError)?;

    println!("PKCS1 - Private");
    println!("{}", key.to_pem_pkcs1()?);
    println!();
    println!("PKCS8 - Private");
    println!("{}", key.to_pem_pkcs8()?);

    let key = key.to_public_key();

    println!("PKCS1 - Public");
    println!("{}", key.to_pem_pkcs1()?);
    println!();
    println!("PKCS8 - Public");
    println!("{}", key.to_pem_pkcs8()?);

    Ok(())
}
