use anyhow::Error;
use rsa::RSAPublicKey;
use rsa_pem::KeyExt;

static ASONIX_PEM: &str = "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwEvEsHqM3twoC2F3KYMQ\n9YOialfVQX4StkLvhLUwFv8qpmY7ZSHHl2TWpnzlo5iWS5Pi2vC41HUGYz9XT5G7\n4IUOyuIkjTL1FIcPJDcUFCzQjN3QZcHLPJPJVNOOOEiOk8//paOyrqJTq9ikcJDM\nJ8KTWQgk1leOxUVEN5uaQ+p9IBFbXC76+RqabfEoqLZagVMDSOfeC2uR9xZ1q5Hk\nFveRTGs84QLR7FJVvx078nszx4UQGnmP0M+0sOeRJGK17IoJmhaok1XBpP6XFQ45\nvYeIRiaFj0Pc9GNISCW70dVXKMhv+K07orQJm6PwP8USyhq4tLkq6tcPbGRqEk3Z\nXwIDAQAB\n-----END PUBLIC KEY-----\n";

#[derive(Clone, Debug, thiserror::Error)]
#[error("Error generating key")]
pub struct MyError;

fn main() -> Result<(), Error> {
    std::env::set_var("RUST_LOG", "debug");
    pretty_env_logger::init();
    log::info!("Parsing:\n{}", ASONIX_PEM);
    RSAPublicKey::from_pem_pkcs8(ASONIX_PEM).map_err(|e| {
        eprintln!("Error, {}, {:?}", e, e);
        MyError
    })?;
    Ok(())
}
