use rsa::RSAPrivateKey;
use rsa_pem::*;

fn main() {
    let mut args = std::env::args();
    let _ = args.next().unwrap();
    let path = args.next().expect("File must be provided");
    let pkcs8 = args.next().is_some();

    let bytes = std::fs::read(path).unwrap();
    let s = String::from_utf8(bytes).unwrap();

    if pkcs8 {
        RSAPrivateKey::from_pem_pkcs8(&s).unwrap();
    } else {
        RSAPrivateKey::from_pem_pkcs1(&s).unwrap();
    }

    println!("Read key!");
}
