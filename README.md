# RSA PEM
_Converting between the PEM format and a RustCrypto RSA type_

This library will be useless after the next release of the RSA crate, which should have this
functionality baked in.

```rust
use rsa_pem::KeyExt as _;
use rsa::{RSAPrivateKey};

let mut rng = rand::thread_rng();
let rsa = RSAPrivateKey::new(&mut rng, 2048)?;

let pem: String = rsa.to_pem_pkcs8()?;

let res = RSAPrivateKey::from_pem_pkcs8(&pem)?;

assert!(res.is_ok());
```

### Contributing
Unless otherwise stated, all contributions to this project will be licensed under the CSL with
the exceptions listed in the License section of this file.

### License
This work is licensed under the Cooperative Software License. This is not a Free Software
License, but may be considered a "source-available License." For most hobbyists, self-employed
developers, worker-owned companies, and cooperatives, this software can be used in most
projects so long as this software is distributed under the terms of the CSL. For more
information, see the provided LICENSE file. If none exists, the license can be found online
[here](https://lynnesbian.space/csl/). If you are a free software project and wish to use this
software under the terms of the GNU Affero General Public License, please contact me at
[asonix@asonix.dog](mailto:asonix@asonix.dog) and we can sort that out. If you wish to use this
project under any other license, especially in proprietary software, the answer is likely no.
